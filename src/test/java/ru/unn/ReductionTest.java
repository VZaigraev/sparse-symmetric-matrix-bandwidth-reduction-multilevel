package ru.unn;

import org.junit.Assert;
import org.junit.Test;
import ru.unn.experiment.MultilevelExperiment;
import ru.unn.loader.ManualMatrixLoader;
import ru.unn.saver.MatrixStatisticSaver;

public class ReductionTest extends Assert {
    private static final String MATRICESPATH = "./test/";

    @Test
    public void manual() {
        MatrixStatisticSaver saver = new MatrixStatisticSaver(MultilevelExperiment.class,
                MultilevelExperiment.headers);
        ILoader loader = new ManualMatrixLoader();
        loader.forEach(e -> {
            IExperiment experiment = new MultilevelExperiment();
            experiment.apply(e);
            saver.save(experiment.collect());
        });
    }

//
//    private Processor createProcessor(ILoader loader) {
//        Processor processor = new Processor();
//        OptimizerBuilder builder = new OptimizerBuilder();
////        builder.appendCutthilMckee(null,    1000)
////                .appendReducingWrapper(OptimizerBuilder.createParallelOptimizer(new BandwidthReductionStrategy(), 10000000L, null, 4),
////                        new BandwidthReductionStrategy());
//        builder.appendCutthilMckee(null, 10000)
//                .appendReducingWrapper(OptimizerBuilder.createParallelOptimizer(new AverageReductionStrategy(), 600L, null, 4),
//                        new AverageReductionStrategy(), 4, 5, 2);
//        ISaver saver = new MatrixStatisticSaver(ReductionTest.class);
//        processor.setLoader(loader);
//        processor.setOptimizer(builder.build());
//        processor.setSaver(saver);
//        return processor;
//    }
//
//    @Test
//    public void manual() {
//        ILoader loader = new ManualMatrixLoader();
//        Processor processor = createProcessor(loader);
//        processor.execute();
//    }
//
//    @Test
//    public void auto() {
//        ILoader loader = new DirectoryMatrixLoader(MATRICESPATH);
//        Processor processor = createProcessor(loader);
//        processor.execute();
//    }
}
