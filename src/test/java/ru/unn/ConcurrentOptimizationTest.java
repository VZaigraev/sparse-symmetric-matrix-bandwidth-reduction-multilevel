package ru.unn;

import org.junit.Test;
import ru.unn.experiment.ParallelExperiment;
import ru.unn.loader.ManualMatrixLoader;
import ru.unn.saver.MatrixStatisticSaver;

public class ConcurrentOptimizationTest {
    private static final String MATRICESPATH = "./test/";

    @Test
    public void manual() {
        MatrixStatisticSaver saver = new MatrixStatisticSaver(ParallelExperiment.class,
                ParallelExperiment.headers);
        ILoader loader = new ManualMatrixLoader();
        loader.forEach(e -> {
            IExperiment experiment = new ParallelExperiment();
            experiment.apply(e);
            saver.save(experiment.collect());
        });
    }

//    private Processor createProcessor(ILoader loader) {
//        Processor processor = new Processor();
//        OptimizerBuilder builder = new OptimizerBuilder();
//        builder.appendCutthilMckee(null,    1000)
//                .appendParallelOptimizer(new AverageReductionStrategy(), 600L, null, 4);
//        ISaver saver = new MatrixStatisticSaver(ConcurrentOptimizationTest.class);
//        processor.setLoader(loader);
//        processor.setOptimizer(builder.build());
//        processor.setSaver(saver);
//        return processor;
//    }
//
//    @Test
//    public void manual() {
//        ILoader loader = new ManualMatrixLoader();
//        Processor processor = createProcessor(loader);
//        processor.execute();
//    }
//
//    @Test
//    public void auto() {
//        ILoader loader = new DirectoryMatrixLoader(MATRICESPATH);
//        Processor processor = createProcessor(loader);
//        processor.execute();
//    }


}
