package ru.unn.matrix;

import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.util.Permutation;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CRSMatrix implements IMatrix {
    private final int[] columns;
    private final int[] indexes;

    public CRSMatrix(int[] columns, int[] indexes) {
        this.columns = columns;
        this.indexes = indexes;
    }

    public Collection<Integer> getRow(final int row) {
        return IntStream.of(Arrays.copyOfRange(columns, indexes[row], indexes[row + 1]))
                .boxed().collect(Collectors.toList());
    }

    public int getSize() {
        return indexes.length - 1;
    }

    public int getDegree(final int row) {
        return indexes[row + 1] - indexes[row];
    }

    @Override
    public IMatrix getComponent() {
        return this;
    }

    @Override
    public IVector getInitialPermutation() {
        return new Permutation(getSize());
    }

}
