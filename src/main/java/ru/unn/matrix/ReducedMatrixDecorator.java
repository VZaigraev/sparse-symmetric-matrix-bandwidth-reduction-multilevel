package ru.unn.matrix;

import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.util.Permutation;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ReducedMatrixDecorator implements IMatrix {
    private final IMatrix component;
    private final Map<Integer, Collection<Integer>> cache = new ConcurrentHashMap<>();

    private final List<List<Integer>> oldIndexes;

    // Region cache for indexOf for indexes
    private final int[] oldToNewIndexes;
    // endregion

    public ReducedMatrixDecorator(IMatrix component, List<List<Integer>> clusters) {
        this.component = component;
        int reduction = clusters.stream().map(List::size).reduce(Integer::sum)
                .orElseThrow(RuntimeException::new) - clusters.size();
        oldIndexes = new ArrayList<>(component.getSize());
        buildArrays(clusters);
        oldToNewIndexes = buildOldToNewIndexesMap();
    }

    private void buildArrays(final List<List<Integer>> clusters) {
        final Set<Integer> processed = new HashSet<>();
        final List<List<Integer>> clustersCopy = new ArrayList<>(clusters);
        for (int c = 0; c < component.getSize(); c++) {
            if (!processed.contains(c)) {
                final List<Integer> next = findClusterContainingIndex(clustersCopy, c);
                if (next != null) {
                    oldIndexes.add(next);
                    processed.addAll(next);
                    clustersCopy.remove(next);
                } else {
                    oldIndexes.add(Collections.singletonList(c));
                }
            }
        }
    }

    private List<Integer> findClusterContainingIndex(final List<List<Integer>> clusters, final int i) {
        return clusters.stream().filter(c -> c.contains(i)).findAny().orElse(null);
    }

    public List<Integer> getOldIndexes(int row) {
        return oldIndexes.get(row);
    }

    private int[] buildOldToNewIndexesMap() {
        int[] result = new int[component.getSize()];
        for (int i = 0; i < oldIndexes.size(); i++) {
            List<Integer> next = oldIndexes.get(i);
            for (int j : next) {
                result[j] = i;
            }
        }
        return result;
    }

    public int[] getOldToNewIndexesMap() {
        return oldToNewIndexes;
    }

    @Override
    public Collection<Integer> getRow(int row) {
        if (cache.get(row) == null) {
            List<Integer> old = oldIndexes.get(row);
            Set<Integer> target = new HashSet<>();
            for (int i : old) {
                component.getRow(i).forEach(j -> target.add(oldToNewIndexes[j]));
            }
            cache.put(row, target);
        }
        return cache.get(row);
    }

    @Override
    public int getSize() {
        return oldIndexes.size();
    }

    @Override
    public int getDegree(int row) {
        return getRow(row).size();
    }

    @Override
    public IMatrix getComponent() {
        return component;
    }

    @Override
    public IVector getInitialPermutation() {
        return new Permutation(getSize());
    }
}
