package ru.unn;

import ru.unn.util.Tuple;

public abstract class BaseWrapper extends BaseOptimizer implements IWrapper {
    private final IOptimizer internal;

    protected BaseWrapper(IOptimizer internal) {
        this.internal = internal;
    }

    @Override
    protected void internalOptimize(final IMatrix value, final IVector oldResult) {
        Tuple<IMatrix, IVector> preHandle = preHandle(value, oldResult);
        final IMatrix reducedMatrix = preHandle.getA();
        final IVector reducedVector = preHandle.getB();

        internal.optimize(reducedMatrix, reducedVector);
        final IVector newReducedVector = internal.getResult();

        Tuple<IMatrix, IVector> postHandle = postHandle(reducedMatrix,
                newReducedVector == null ? reducedVector : newReducedVector);
        result.set(postHandle.getB());
    }
}
