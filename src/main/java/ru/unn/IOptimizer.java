package ru.unn;

public interface IOptimizer {
    void optimize(IMatrix value, IVector oldResult);

    IVector getResult();
}
