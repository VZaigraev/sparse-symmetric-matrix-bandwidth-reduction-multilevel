package ru.unn.saver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

public class MatrixStatisticSaver {
    private final File file;
    private String[] headers;

    public MatrixStatisticSaver(Class clazz, String[] headers) {
        this("statistics." + new Date().toString() + '.' + clazz.getName() + ".csv", headers);
    }

    public MatrixStatisticSaver(String fileName, String[] headers) {
        this.file = new File("./result/" + fileName);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(buildHeader(headers));
        } catch (IOException e) {
            throw new RuntimeException("File writing failed", e);
        }
    }

    private String buildHeader(String[] headers) {
        this.headers = headers;
        return Arrays.stream(headers).collect(Collectors.joining(";"));
    }

    private String buildRow(final Map<String, String> result) {
        final String[] row = new String[headers.length];
        for (int i = 0; i < headers.length; i++) {
            row[i] = result.get(headers[i]);
        }
        return Arrays.stream(row).map(String::valueOf).collect(Collectors.joining(";"));
    }

    public void save(final Map<String, String> result) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            writer.newLine();
            writer.write(buildRow(result));
        } catch (IOException e) {
            throw new RuntimeException("File writing failed", e);
        }
    }
}
