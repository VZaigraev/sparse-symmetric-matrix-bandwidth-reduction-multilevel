package ru.unn;

import java.util.Map;

public interface IStatisticsCollector {
    Map<String, String> collect();

}
