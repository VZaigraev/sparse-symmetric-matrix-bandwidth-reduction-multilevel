package ru.unn.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.ILoader;
import ru.unn.IMatrix;
import ru.unn.util.EntityWithName;
import ru.unn.util.LoadMatrix;

import java.awt.*;
import java.io.File;
import java.util.function.Consumer;

public class ManualMatrixLoader implements ILoader {
    private static final Logger logger = LoggerFactory.getLogger(ManualMatrixLoader.class);
    private File[] files;
    private int cursor;

    public ManualMatrixLoader() {
        cursor = 0;
        files = null;
        final FileDialog fileopen = new FileDialog((Frame) null, "Choose files", FileDialog.LOAD);
        fileopen.setDirectory("./");
        fileopen.setMultipleMode(true);
        fileopen.setVisible(true);
        final File[] selectedFiles = fileopen.getFiles();
        if (selectedFiles != null && selectedFiles.length > 0) {
            files = selectedFiles;
        }
    }

    private boolean hasNext() {
        return files != null && cursor < files.length;
    }

    private EntityWithName<IMatrix> load() {
        final File file = files[cursor++];
        logger.info(String.format("Loading file: %s", file.getName()));
        final IMatrix matrix = LoadMatrix.loadMatrix(file);
        logger.info("Finished loading");
        return new EntityWithName<>(file.getName(), matrix);
    }


    @Override
    public void forEach(Consumer<EntityWithName<IMatrix>> consumer) {
        while (hasNext()) {
            try {
                consumer.accept(load());
            } catch (RuntimeException e) {
                logger.error("Exception during processing", e);
            }
        }
    }
}
