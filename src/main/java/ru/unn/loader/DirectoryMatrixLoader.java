package ru.unn.loader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.ILoader;
import ru.unn.IMatrix;
import ru.unn.util.EntityWithName;
import ru.unn.util.LoadMatrix;

import java.io.File;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class DirectoryMatrixLoader implements ILoader {
    private static final Logger logger = LoggerFactory.getLogger(ManualMatrixLoader.class);
    private File[] files;
    private int cursor;

    public DirectoryMatrixLoader(final String path) {
        File directory = new File(path);
        if (directory.isDirectory()) {
            files = directory.listFiles();
            if (files != null) {
                files = Arrays.stream(files).filter(isMtx()).toArray(File[]::new);
            } else {
                logger.error("Directory is empty");
            }
        }
        cursor = 0;
    }

    private Predicate<File> isMtx() {
        return file -> file.getName().matches(".*\\.mtx");
    }

    private boolean hasNext() {
        return files != null && cursor < files.length;
    }

    private EntityWithName<IMatrix> load() {
        final File file = files[cursor++];
        logger.info(String.format("Loading file: %s", file.getName()));
        final IMatrix matrix = LoadMatrix.loadMatrix(file);
        logger.info("Finished loading");
        return new EntityWithName<>(file.getName(), matrix);
    }

    @Override
    public void forEach(Consumer<EntityWithName<IMatrix>> consumer) {
        while (hasNext()) {
            try {
                consumer.accept(load());
            } catch (RuntimeException e) {
                logger.error("Exception during processing", e);
            }
        }
    }
}
