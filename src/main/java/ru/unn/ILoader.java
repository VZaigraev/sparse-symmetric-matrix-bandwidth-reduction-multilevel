package ru.unn;

import ru.unn.util.EntityWithName;

import java.util.function.Consumer;

public interface ILoader {
    void forEach(Consumer<EntityWithName<IMatrix>> consumer);
}
