package ru.unn;

import ru.unn.util.EntityWithName;

public interface IExperiment extends IStatisticsCollector {
    void apply(EntityWithName<IMatrix> matrix);

    IVector getPermutation();
}
