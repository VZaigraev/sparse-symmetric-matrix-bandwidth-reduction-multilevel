package ru.unn;

import ru.unn.util.Tuple;

public interface IWrapper extends IOptimizer {
    Tuple<IMatrix, IVector> preHandle(IMatrix value, IVector oldResult);

    Tuple<IMatrix, IVector> postHandle(IMatrix value, IVector oldResult);

}
