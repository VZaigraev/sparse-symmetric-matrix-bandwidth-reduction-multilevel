package ru.unn;

import java.io.Serializable;
import java.util.Collection;

public interface IMatrix extends Serializable {
    Collection<Integer> getRow(final int row);

    int getSize();

    int getDegree(int row);

    IMatrix getComponent();

    IVector getInitialPermutation();

}
