package ru.unn.optimizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.BaseOptimizer;
import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.util.Permutation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class CuthillMckeeScheme extends BaseOptimizer {
    private static final Logger logger = LoggerFactory.getLogger(CuthillMckeeScheme.class);
    private final int maxIterations;

    public CuthillMckeeScheme(int maxIterations) {
        this.maxIterations = maxIterations;
    }

    @Override
    protected void internalOptimize(IMatrix matrix, IVector oldPermutation) {
        result.set(findSubGraphs(matrix));
    }

    private IVector findSubGraphs(IMatrix matrix) {
        final List<Future<int[]>> futureList = new ArrayList<>();
        final ExecutorService service = Executors.newCachedThreadPool();
        final IVector permutation = matrix.getInitialPermutation();
        final AtomicInteger cursor = new AtomicInteger();
        while (cursor.get() < matrix.getSize()) {
            int levelStart = cursor.get();
            int lastLevelStart = levelStart;
            int lastLevelEnd = cursor.incrementAndGet();
            while (cursor.get() < matrix.getSize()) {
                for (int i = lastLevelStart; i < lastLevelEnd; i++) {
                    matrix.getRow(permutation.get(i))
                            .forEach(v -> {
                                final int index = permutation.indexOf(v);
                                if (index >= cursor.get()) {
                                    permutation.swap(index, cursor.getAndIncrement());
                                }
                            });
                }
                if (lastLevelEnd == cursor.get()) break;
                permutation.sort(lastLevelEnd, cursor.get(), Comparator.comparingInt(matrix::getDegree));
                lastLevelStart = lastLevelEnd;
                lastLevelEnd = cursor.get();
            }
            if (levelStart < cursor.get()) {
                int finalCursor = cursor.get();
                futureList.add(service.submit(() -> solveTaskPartly(matrix, permutation, levelStart, finalCursor)));
            }
        }
        try {
            service.shutdown();
            service.awaitTermination(10, TimeUnit.MINUTES);
            int[] newPermutation = new int[permutation.getSize()];
            for (int i = 0, j = 0; i < futureList.size(); i++) {
                for (int value : futureList.get(i).get()) {
                    newPermutation[j++] = value;
                }
            }
            return new Permutation(newPermutation);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    private int[] solveTaskPartly(final IMatrix matrix, final IVector oldPermutation, final int startIndex, final int endIndex) {
        if (startIndex + 1 == endIndex)
            return new int[]{oldPermutation.get(startIndex)};
        IVector permutation = null;
        int candidateIndex = startIndex;
        int previousLevels = -1;
        int iteration = 0;
        while (iteration++ < maxIterations) {
            IVector cur = oldPermutation.copyInstance();
            int lastLevelStart = startIndex;
            int lastLevelEnd = startIndex + 1;
            final AtomicInteger cursor = new AtomicInteger(startIndex + 1);
            int levels = 1;
            cur.swap(candidateIndex, startIndex);
            while (cursor.get() < endIndex) {
                for (int i = lastLevelStart; i < lastLevelEnd; i++) {
                    matrix.getRow(cur.get(i))
                            .forEach(v -> {
                                final int index = cur.indexOf(v);
                                if (index >= cursor.get() && index >= startIndex && index <= endIndex) {
                                    cur.swap(index, cursor.getAndIncrement());
                                }
                            });
                }
                if (lastLevelEnd == cursor.get()) break;
                cur.sort(lastLevelEnd, cursor.get(), Comparator.comparingInt(matrix::getDegree));
                lastLevelStart = lastLevelEnd;
                lastLevelEnd = cursor.get();
                levels++;
            }
            if (levels > previousLevels) {
                candidateIndex = cur.get(lastLevelStart);
                previousLevels = levels;
                permutation = cur;
            } else {
                break;
            }
        }
        if (permutation == null) {
            logger.error("No permutation generated");
            throw new RuntimeException("No permutation generated");
        }
        return permutation.copyOfRange(startIndex, endIndex);
    }
}
