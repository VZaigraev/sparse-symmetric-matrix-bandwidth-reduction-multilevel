package ru.unn.optimizer.local;

import ru.unn.BaseOptimizer;
import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.optimizer.local.strategy.IOptimizationStrategy;
import ru.unn.util.MatrixStatistics;

public abstract class LocalOptimizer extends BaseOptimizer {
    private final IOptimizationStrategy optimizationStrategy;

    public LocalOptimizer(IOptimizationStrategy optimizationStrategy) {
        this.optimizationStrategy = optimizationStrategy;
    }

    protected boolean isSwapNeeded(final IMatrix matrix, final IVector permutation,
                                   final int a, final int b) {
        return optimizationStrategy.isSwapNeeded(matrix, permutation, a, b);
    }

    protected int getMaxLocalBandwidthOfPair(final IMatrix matrix, final IVector permutation, final int a, final int b) {
        return Integer.max(MatrixStatistics.getLocalBandwidth(matrix, permutation, a),
                MatrixStatistics.getLocalBandwidth(matrix, permutation, b));
    }

}
