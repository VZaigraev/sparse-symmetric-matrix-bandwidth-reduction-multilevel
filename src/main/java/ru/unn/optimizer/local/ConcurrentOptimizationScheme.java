package ru.unn.optimizer.local;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.optimizer.local.strategy.IOptimizationStrategy;
import ru.unn.util.MatrixStatistics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class ConcurrentOptimizationScheme extends LocalOptimizer {
    private static final Logger logger = LoggerFactory.getLogger(ConcurrentOptimizationScheme.class);
    private final Integer from;
    private final Integer to;
    private final int threads;
    private Integer iterationsLimit;

    public ConcurrentOptimizationScheme(IOptimizationStrategy optimizationStrategy, int threads) {
        this(optimizationStrategy, threads, null, null);
    }

    public ConcurrentOptimizationScheme(IOptimizationStrategy optimizationStrategy, int threads, Integer from, Integer to) {
        super(optimizationStrategy);
        this.threads = threads;
        this.from = from;
        this.to = to;
    }

    @Override
    protected void internalOptimize(IMatrix matrix, IVector oldPermutation) {
        final IVector permutation = oldPermutation.copyInstance();
        result.set(permutation);

        boolean hasMore = true;
        final int from = this.from == null ? 0 : this.from;
        final int to = this.to == null ? permutation.getSize() : this.to;
        final int size = to - from;
        final int gapSize = MatrixStatistics.getBandwidth(matrix, permutation);
        int totalGapNumber = size / gapSize + 1;
        final Boolean[] gapActuality = new Boolean[totalGapNumber];
        Arrays.fill(gapActuality, true);
        if (loggerEnabled)
            logger.info("Step [0], Bandwidth: {}, Average: {}",
                    MatrixStatistics.getBandwidth(matrix, permutation),
                    MatrixStatistics.getAverageBandwidth(matrix, permutation));
        for (int iteration = 1; hasMore && isActive(iteration); iteration++) {
            if (loggerEnabled) {
                logger.info("Step [{}], Bandwidth: {}, Average: {}",
                        iteration,
                        MatrixStatistics.getBandwidth(matrix, permutation),
                        MatrixStatistics.getAverageBandwidth(matrix, permutation));
            }
            for (int j = 0; j < 2; j++) {
                optimizeGapList(from, to, j, gapActuality, matrix, permutation, gapSize);
            }
            hasMore = Arrays.stream(gapActuality).reduce(Boolean::logicalOr).orElse(false);
        }
    }

    private boolean isActive(int iteration) {
        return isActive() && (iterationsLimit == null || iteration < iterationsLimit);
    }

    private void optimizeGapList(final int from, final int to,
                                 final int startIndex, final Boolean[] gapActuality,
                                 final IMatrix matrix, final IVector permutation, final int gapSize) {
        final ExecutorService executorService = Executors.newFixedThreadPool(threads);
        final List<Callable<Void>> tasks = new ArrayList<>();
        final int size = to - from;
        for (int i = startIndex; i < gapActuality.length; i += 2) {
            if (gapActuality[i]) {
                final int index = from + i * gapSize;
                gapActuality[i] = false;
                final int finalI = i;
                final Callable<Void> task = () -> {
                    int upperBound = Integer.min(index + gapSize, size - 1);
                    for (int r = index; r < upperBound; r++) {
                        if (isSwapNeeded(matrix, permutation, r, r + 1)) {
                            permutation.swap(r, r + 1);
                            gapActuality[finalI] = true;
                            if (finalI - 1 > 0)
                                gapActuality[finalI - 1] = true;
                            if (finalI + 1 < gapActuality.length)
                                gapActuality[finalI + 1] = true;
                        }
                    }
                    return null;
                };
                tasks.add(task);
            }
        }
        try {
            List<Future<Void>> results = executorService.invokeAll(tasks);
            executorService.shutdown();
            for (Future<Void> result : results) {
                result.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            logger.warn("Work interrupted", e);
            executorService.shutdownNow();
        }
    }

    public void setIterationsLimit(Integer iterationsLimit) {
        this.iterationsLimit = iterationsLimit;
    }
}
