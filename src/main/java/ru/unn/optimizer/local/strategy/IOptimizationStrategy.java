package ru.unn.optimizer.local.strategy;

import ru.unn.IMatrix;
import ru.unn.IVector;

public interface IOptimizationStrategy {
    boolean isSwapNeeded(final IMatrix matrix, final IVector permutation, final int a, final int b);
}
