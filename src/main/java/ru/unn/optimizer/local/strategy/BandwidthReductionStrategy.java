package ru.unn.optimizer.local.strategy;

import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.util.MatrixStatistics;
import ru.unn.util.SwappedVectorDecorator;

public class BandwidthReductionStrategy implements IOptimizationStrategy {

    @Override
    public boolean isSwapNeeded(IMatrix matrix, IVector permutation, int a, int b) {
        int max = getMaxLocalBandwidthOfPair(matrix, permutation, a, b);
        int newMax = getMaxLocalBandwidthOfSwappedPair(matrix, permutation, a, b);
        return max > newMax;
    }

    private int getMaxLocalBandwidthOfPair(final IMatrix matrix, final IVector permutation, final int a, final int b) {
        return Integer.max(MatrixStatistics.getLocalBandwidth(matrix, permutation, a),
                MatrixStatistics.getLocalBandwidth(matrix, permutation, b));
    }

    private int getMaxLocalBandwidthOfSwappedPair(final IMatrix matrix, final IVector permutation, final int a, final int b) {
        final IVector swappedPermutation = new SwappedVectorDecorator(permutation, a, b);
        return Integer.max(MatrixStatistics.getLocalBandwidth(matrix, swappedPermutation, a),
                MatrixStatistics.getLocalBandwidth(matrix, swappedPermutation, b));
    }

}
