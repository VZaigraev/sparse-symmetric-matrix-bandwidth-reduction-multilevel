package ru.unn.optimizer.local.strategy;

import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.util.MatrixStatistics;
import ru.unn.util.SwappedVectorDecorator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AverageReductionStrategy implements IOptimizationStrategy {

    @Override
    public boolean isSwapNeeded(final IMatrix matrix, final IVector permutation, final int a, final int b) {
        final IVector swappedPermutation = new SwappedVectorDecorator(permutation, a, b);
        final int aReal = permutation.get(a);
        final int bReal = permutation.get(b);
        final Set<Integer> affected = Stream.of(matrix.getRow(aReal), matrix.getRow(bReal))
                .flatMap(Collection::stream)
                .distinct()
                .map(permutation::indexOf)
                .collect(Collectors.toSet());
        affected.add(a);
        affected.add(b);
        double average = affected.stream().map(v -> MatrixStatistics.getLocalBandwidth(matrix, permutation, v))
                .map(BigDecimal::valueOf)
                .reduce(BigDecimal::add)
                .map(s -> s.divide(BigDecimal.valueOf(affected.size()), 20, RoundingMode.HALF_UP))
                .map(BigDecimal::doubleValue)
                .orElse(0.d);
        double newAverage = affected.stream().map(v -> MatrixStatistics.getLocalBandwidth(matrix, swappedPermutation, v))
                .map(BigDecimal::valueOf)
                .reduce(BigDecimal::add)
                .map(s -> s.divide(BigDecimal.valueOf(affected.size()), 20, RoundingMode.HALF_UP))
                .map(BigDecimal::doubleValue)
                .orElse(0.d);
        return average > newAverage;
    }

}
