package ru.unn.optimizer.local.strategy;

import ru.unn.IMatrix;
import ru.unn.IVector;

public class DoNothingStrategy implements IOptimizationStrategy {
    @Override
    public boolean isSwapNeeded(IMatrix matrix, IVector permutation, int a, int b) {
        return false;
    }
}
