package ru.unn.optimizer.local;

import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.optimizer.local.strategy.IOptimizationStrategy;

public class SubsequentOptimizationScheme extends LocalOptimizer {
    private final Integer from;
    private final Integer to;
    private Integer tactLimit = Integer.MAX_VALUE;

    public SubsequentOptimizationScheme(IOptimizationStrategy optimizationStrategy) {
        this(optimizationStrategy, null, null);
    }

    public SubsequentOptimizationScheme(IOptimizationStrategy optimizationStrategy, Integer from, Integer to) {
        super(optimizationStrategy);
        this.from = from;
        this.to = to;
    }

    @Override
    protected void internalOptimize(final IMatrix matrix, final IVector oldPermutation) {
        IVector permutation = oldPermutation.copyInstance();
        result.set(permutation);

        final int from = this.from == null ? 0 : this.from;
        final int to = this.to == null ? permutation.getSize() : this.to;
        validate(from, to, permutation.getSize());
        for (int i = from, tact = 0; i < to - 1 && isActive(tact); tact++, i++) {
            if (isSwapNeeded(matrix, permutation, i, i + 1)) {
                permutation.swap(i, i + 1);
                i = Integer.max(from - 1,
                        i - getMaxLocalBandwidthOfPair(matrix, permutation, i, i + 1));
            }
        }
    }

    private void validate(final int from, final int to, final int max) {
        if (from > to) {
            throw new IndexOutOfBoundsException("From index is bigger than to index");
        }
        if (from < 0) {
            throw new IndexOutOfBoundsException("From index cannot be negative");
        }
        if (to > max) {
            throw new IndexOutOfBoundsException("To index cannot exceed max index");
        }
    }

    protected boolean isActive(final int tact) {
        return isActive() && (tactLimit == null || tact < tactLimit);
    }

    public void setTactLimit(final Integer tactLimit) {
        this.tactLimit = tactLimit;
    }
}
