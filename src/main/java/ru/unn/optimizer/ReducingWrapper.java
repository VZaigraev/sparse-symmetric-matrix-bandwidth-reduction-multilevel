package ru.unn.optimizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.*;
import ru.unn.matrix.ReducedMatrixDecorator;
import ru.unn.optimizer.local.ConcurrentOptimizationScheme;
import ru.unn.optimizer.local.LocalOptimizer;
import ru.unn.optimizer.local.SubsequentOptimizationScheme;
import ru.unn.optimizer.local.strategy.IOptimizationStrategy;
import ru.unn.util.Permutation;
import ru.unn.util.Tuple;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
public class ReducingWrapper extends BaseWrapper {
    private static final Logger logger = LoggerFactory.getLogger(BaseOptimizer.class);

    private final int iterations;
    private final int threads;
    private final int threshold;
    private final int reductionLimit;
    private final IOptimizationStrategy optimizationStrategy;

    public ReducingWrapper(IOptimizer internal, int iterations, int threads, int threshold, int reductionLimit, IOptimizationStrategy optimizationStrategy) {
        super(internal);
        this.iterations = iterations;
        this.threads = threads;
        this.threshold = threshold;
        this.reductionLimit = reductionLimit;
        this.optimizationStrategy = optimizationStrategy;
    }

    // region PreHandle
    @Override
    public Tuple<IMatrix, IVector> preHandle(final IMatrix oldMatrix, final IVector oldPermutation) {
        IVector permutation = oldPermutation;
        IMatrix matrix = oldMatrix;
        for (int i = 0; i < iterations && matrix.getSize() > reductionLimit; i++) {
            final IVector finalPermutation = permutation;
            List<List<Integer>> clusters = Stream.of(findClusters(matrix, permutation))
                    .map(this::filterClusters)
                    .flatMap(Collection::stream)
                    .map(r -> createClusterList(r, finalPermutation))
                    .collect(Collectors.toList());
            if (!clusters.isEmpty()) {
                logger.info(String.format("Iteration %d. Matrix size %d.", i, matrix.getSize()));
                matrix = new ReducedMatrixDecorator(matrix, clusters);
                permutation = reducePermutation((ReducedMatrixDecorator) matrix, permutation);
                logger.info(String.format("Iteration %d. Reduced %d clusters. Matrix size %d.", i, clusters.size(), matrix.getSize()));
            } else {
                logger.info(String.format("Iteration %d. No more clusters found.", i));
                break;
            }
        }
        return new Tuple<>(matrix, permutation);
    }

    private List<Integer> createClusterList(Tuple<Integer, Integer> range, IVector permutation) {
        List<Integer> result = new ArrayList<>();
        int start = range.getA();
        int end = range.getA() + range.getB();
        for (int i = start; i < end; i++) {
            result.add(permutation.get(i));
        }
        return result;
    }

    private Collection<Tuple<Integer, Integer>> findClusters(final IMatrix matrix, final IVector permutation) {
        final Set<Tuple<Integer, Integer>> target = new HashSet<>();
        final ExecutorService executorService = Executors.newFixedThreadPool(threads);
        int[] gaps = new int[threads + 1];
        int step = matrix.getSize() / threads;
        for (int i = 0; i < threads; i++) {
            gaps[i] = step * i;
        }
        gaps[threads] = matrix.getSize();
        final List<Callable<Void>> tasks = new ArrayList<>();
        for (int i = 0; i < threads; i++) {
            final int finalI = i;
            Callable<Void> task = () -> {
                for (int j = gaps[finalI]; j < gaps[finalI + 1]; j++) {
                    Tuple<Integer, Integer> left = getLeftCluster(matrix, permutation, j);
                    if (left != null && left.getB() > threshold) {
                        synchronized (target) {
                            target.add(left);
                        }
                    }
//                    Tuple<Integer, Integer> right = getRightCluster(matrix, permutation, j);
//                    if (right != null && right.getB() > threshold) {
//                        synchronized (target) {
//                            target.add(right);
//                        }
//                    }
                }
                return null;
            };
            tasks.add(task);
        }
        try {
            List<Future<Void>> results = executorService.invokeAll(tasks);
            executorService.shutdown();
            for (Future<Void> result : results) {
                result.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            logger.warn("Work interrupted", e);
            executorService.shutdownNow();
        }
        return target;
    }

    private Collection<Tuple<Integer, Integer>> filterClusters(Collection<Tuple<Integer, Integer>> clusters) {
        final Set<Integer> used = new HashSet<>();
        return clusters.stream().sorted(Comparator.comparing(Tuple::getB, Comparator.reverseOrder()))
                .map(t -> validate(t, used))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Tuple<Integer, Integer> validate(final Tuple<Integer, Integer> candidate, final Set<Integer> used) {
        Tuple<Integer, Integer> result = candidate;
        final int start = candidate.getA();
        final int end = candidate.getA() + candidate.getB();
        synchronized (used) {
            for (int i = start; i < end; i++) {
                if (used.contains(i)) {
                    result = null;
                }
            }
            if (result != null) {
                register(candidate, used);
            }
        }
        return result;
    }

    private void register(Tuple<Integer, Integer> candidate, Set<Integer> used) {
        final int start = candidate.getA();
        final int end = candidate.getA() + candidate.getB();
        for (int i = start; i < end; i++) {
            used.add(i);
        }
    }

    private Tuple<Integer, Integer> getLeftCluster(final IMatrix matrix, final IVector permutation, final int row) {
        int degree = matrix.getDegree(row);
        int index = permutation.indexOf(row);
        final List<Integer> indexes = new ArrayList<>();
        matrix.getRow(row).forEach(i -> indexes.add(permutation.indexOf(i)));
        indexes.sort(Comparator.naturalOrder());
        if (indexes.size() > 1) {
            int i = 0;
            while (i + 1 < indexes.size() &&
                    indexes.get(i) + 1 == indexes.get(i + 1) &&
                    indexes.get(i) < index) {
                i++;
            }
            if (i > 0) {
                return new Tuple<>(indexes.get(0), i);
            }
        }
        return null;
    }

    private Tuple<Integer, Integer> getRightCluster(final IMatrix matrix, final IVector permutation, final int row) {
        int degree = matrix.getDegree(row);
        int index = permutation.indexOf(row);
        final List<Integer> indexes = new ArrayList<>();
        matrix.getRow(row).forEach(i -> indexes.add(permutation.indexOf(i)));
        indexes.sort(Comparator.naturalOrder());
        if (indexes.size() > 1) {
            int i = indexes.size() - 1;
            while (i > 0 &&
                    indexes.get(i) - 1 == indexes.get(i - 1) &&
                    indexes.get(i) > index) {
                i--;
            }
            if (i < indexes.size() - 1) {
                return new Tuple<>(indexes.get(i), indexes.size() - i);
            }
        }
        return null;
    }

    private IVector reducePermutation(final ReducedMatrixDecorator matrix, final IVector permutation) {
        int[] mapping = matrix.getOldToNewIndexesMap();
        int[] newPermutation = new int[matrix.getSize()];
        int i = 0, j = 0;
        while (i < permutation.getSize() && j < newPermutation.length) {
            newPermutation[j++] = mapping[permutation.get(i)];
            while (i < permutation.getSize() && j < newPermutation.length && mapping[permutation.get(i)] == newPermutation[j - 1]) {
                i++;
            }
        }
        return new Permutation(newPermutation);
    }
    // endregion

    // region PostHandle
    @Override
    public Tuple<IMatrix, IVector> postHandle(IMatrix oldMatrix, IVector oldPermutation) {
        IMatrix matrix = oldMatrix;
        IVector permutation = oldPermutation;
        for (int i = 0; i < iterations && matrix.getComponent() != matrix; i++) {
            logger.info(String.format("Iteration %d. Reduced matrix size %d.", i, matrix.getSize()));
            if (matrix instanceof ReducedMatrixDecorator) {
                permutation = buildNewPermutation((ReducedMatrixDecorator) matrix, permutation);
            }
            matrix = matrix.getComponent();
            logger.info(String.format("Iteration %d. Restored matrix size %d.", i, matrix.getSize()));
        }
        return new Tuple<>(matrix, permutation);
    }

    private IVector buildNewPermutation(final ReducedMatrixDecorator decorator, final IVector oldPermutation) {
        final int maxIndex = decorator.getComponent().getSize();
        final int[] permutation = new int[maxIndex];
        List<Tuple<Integer, Integer>> reducedClusters = new ArrayList<>();
        for (int i = 0, j = 0; i < permutation.length && j < oldPermutation.getSize(); j++) {
            final List<Integer> indexes = decorator.getOldIndexes(oldPermutation.get(j));
            if (indexes.size() > 1) {
                reducedClusters.add(new Tuple<>(i, indexes.size()));
            }

            for (int index : indexes) {
                permutation[i++] = index;
            }
        }
        IVector result = new Permutation(permutation);
        if (!reducedClusters.isEmpty()) {
            for (Tuple<Integer, Integer> cluster : reducedClusters) {
                result = optimizeCluster(decorator.getComponent(), result, cluster.getA(),
                        cluster.getA() + cluster.getB());
            }
        }
        return result;
    }

    private IVector optimizeCluster(final IMatrix matrix, final IVector permutation, final int from, final int to) {
        final LocalOptimizer optimizer;
        if (to - from < 100) {
            optimizer = new SubsequentOptimizationScheme(optimizationStrategy, from, to);
            ((SubsequentOptimizationScheme) optimizer).setTactLimit((to - from) * (to - from));
        } else {
            optimizer = new ConcurrentOptimizationScheme(optimizationStrategy, threads, from, to);
        }
        optimizer.setLoggerEnabled(false);
        optimizer.setTimeLimit(5L);
        optimizer.optimize(matrix, permutation);
        return optimizer.getResult();
    }
    // endregion
}
