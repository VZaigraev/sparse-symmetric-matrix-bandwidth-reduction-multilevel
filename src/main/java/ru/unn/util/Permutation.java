package ru.unn.util;

import ru.unn.IVector;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.IntStream;

public class Permutation implements IVector {
    private int[] values = null;
    private int[] indexes = null;
    private final Object lock = new Object();

    public Permutation(final int[] permutation) {
        values = Arrays.copyOf(permutation, permutation.length);
        indexes = new int[values.length];
        Arrays.fill(indexes, -1);
        for (int j = 0; j < indexes.length; j++) {
            if (indexes[values[j]] != -1) {
                throw new RuntimeException("Permutation is broken");
            }
            indexes[values[j]] = j;
        }
    }

    public Permutation(final Permutation permutation) {
        this(permutation.values);
    }

    public Permutation(final int size) {
        this(IntStream.iterate(0, a -> a + 1).limit(size).toArray());
    }

    @Override
    public void swap(final int i, final int j) {
        synchronized (lock) {
            indexes[values[i]] = j;
            indexes[values[j]] = i;
            final int buffer = values[i];
            values[i] = values[j];
            values[j] = buffer;
        }
    }

    @Override
    public void sort(final int from, final int to, final Comparator<Integer> comparator) {
        synchronized (lock) {
            for (int i = from; i < to; i++) {
                for (int j = from; j < to - i; j++) {
                    if (comparator.compare(values[i], values[j]) > 0) {
                        swap(i, j);
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Permutation{" +
                Arrays.toString(values) +
                '}';
    }

    @Override
    public int getSize() {
        return values.length;
    }

    @Override
    public int indexOf(int value) {
        return indexes[value];
    }

    @Override
    public int get(final int index) {
        return values[index];
    }

    @Override
    public int[] copyOfRange(int from, int to) {
        synchronized (lock) {
            return Arrays.copyOfRange(values, from, to);
        }
    }

    @Override
    public IVector copyInstance() {
        synchronized (lock) {
            return new Permutation(this);
        }
    }
}
