package ru.unn.util;

import java.io.Serializable;

public class EntityWithName<T extends Serializable> {
    private final String name;
    private final T data;

    public EntityWithName(String name, T data) {
        this.name = name;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public T getData() {
        return data;
    }
}
