package ru.unn.util;

import ru.unn.IMatrix;
import ru.unn.IVector;

public class PermutationHolder {
    private final IMatrix matrix;
    private final IVector vector;
    private final double average;
    private final int bandwidth;

    public PermutationHolder(final IMatrix matrix, final IVector vector) {
        this.matrix = matrix;
        this.vector = vector;
        average = MatrixStatistics.getAverageBandwidth(matrix, vector);
        bandwidth = MatrixStatistics.getBandwidth(matrix, vector);
    }

    public IMatrix getMatrix() {
        return matrix;
    }

    public IVector getVector() {
        return vector;
    }

    public double getAverage() {
        return average;
    }

    public int getBandwidth() {
        return bandwidth;
    }

    public static PermutationHolder bestByAverage(PermutationHolder a, PermutationHolder b) {
        return a.average < b.average ? a : b;
    }

    public static PermutationHolder bestByBandwidth(PermutationHolder a, PermutationHolder b) {
        return a.bandwidth < b.bandwidth ? a : b;
    }
}
