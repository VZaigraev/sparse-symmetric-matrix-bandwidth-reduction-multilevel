package ru.unn.util;

import ru.unn.IMatrix;
import ru.unn.matrix.CRSMatrix;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MatrixBuilder {
    private final List<Set<Integer>> verticesList;

    public MatrixBuilder(final int size) {
        verticesList = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            verticesList.add(new HashSet<>());
        }
    }

    public void addItem(final int a, final int b) {
        if (a == b) return;
        final Set<Integer> aVertices = verticesList.get(a);
        final Set<Integer> bVertices = verticesList.get(b);
        synchronized (aVertices) {
            aVertices.add(b);
        }
        synchronized (bVertices) {
            bVertices.add(a);
        }
    }

    public IMatrix buildMatrix() {
        final int[] values = new int[calculateValuesSize()];
        final int[] indexes = new int[verticesList.size() + 1];
        indexes[0] = 0;
        int indexesIndex = 0, verticesIndex = 0;
        while (verticesIndex < verticesList.size()) {
            final Set<Integer> vertices = verticesList.get(verticesIndex);
            for (Integer value : vertices) {
                values[indexesIndex++] = value;
            }
            indexes[++verticesIndex] = indexesIndex;
        }
        return new CRSMatrix(values, indexes);
    }

    private int calculateValuesSize() {
        return verticesList.stream().map(Set::size).reduce(Integer::sum).orElse(0);
    }

}
