package ru.unn.util;

import ru.unn.IVector;

import java.util.Comparator;

public class SwappedVectorDecorator implements IVector {
    private final IVector component;
    private final int a;
    private final int b;

    public SwappedVectorDecorator(IVector component, int a, int b) {
        this.component = component;
        this.a = a;
        this.b = b;
    }

    public IVector getComponent() {
        return component;
    }

    @Override
    public int getSize() {
        return component.getSize();
    }

    @Override
    public int indexOf(int value) {
        int result = component.indexOf(value);
        if (result == a) {
            result = b;
        } else if (result == b) {
            result = a;
        }
        return result;
    }

    @Override
    public int get(int index) {
        if (index == a) {
            index = b;
        } else if (index == b) {
            index = a;
        }
        return component.get(index);
    }

    @Override
    public void swap(int a, int b) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sort(int from, int to, Comparator<Integer> comparator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int[] copyOfRange(int from, int to) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IVector copyInstance() {
        return this;
    }
}
