package ru.unn.util;

import ru.unn.IMatrix;
import ru.unn.IVector;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public abstract class MatrixStatistics {
    public static int getLocalBandwidth(final IMatrix matrix, final int index) {
        AtomicInteger result = new AtomicInteger(0);
        matrix.getRow(index).forEach(v -> {
            int dif = Math.abs(v - index);
            if (dif > result.get()) {
                result.set(dif);
            }
        });
        return result.get();
    }

    public static int getLocalBandwidth(final IMatrix matrix, final IVector permutation, final int index) {
        AtomicInteger result = new AtomicInteger(0);
        final int realIndex = permutation.get(index);
        matrix.getRow(realIndex).forEach(v -> {
            int dif = Math.abs(permutation.indexOf(v) - index);
            if (dif > result.get()) {
                result.set(dif);
            }
        });
        return result.get();
    }

    public static int getBandwidth(final IMatrix matrix) {
        return IntStream.iterate(0, a -> a + 1)
                .limit(matrix.getSize())
                .map(row -> getLocalBandwidth(matrix, row))
                .max().orElseThrow(() -> new RuntimeException("Exception during bandwidth calculation"));
    }

    public static int getBandwidth(final IMatrix matrix, final IVector permutation) {
        return IntStream.iterate(0, a -> a + 1)
                .limit(matrix.getSize())
                .map(row -> getLocalBandwidth(matrix, permutation, row))
                .max().orElseThrow(() -> new RuntimeException("Exception during bandwidth calculation"));
    }

    public static double getAverageBandwidth(final IMatrix matrix) {
        return Stream.iterate(0, a -> a + 1)
                .limit(matrix.getSize())
                .map(row -> getLocalBandwidth(matrix, row))
                .map(BigDecimal::valueOf)
                .reduce(BigDecimal::add)
                .map(v -> v.divide(BigDecimal.valueOf(matrix.getSize()), 20, RoundingMode.HALF_UP))
                .map(BigDecimal::doubleValue)
                .orElseThrow(() -> new RuntimeException("Exception during average bandwidth calculation"));
    }

    public static double getAverageBandwidth(final IMatrix matrix, final IVector permutation) {
        return Stream.iterate(0, a -> a + 1)
                .limit(matrix.getSize())
                .map(row -> (double) getLocalBandwidth(matrix, permutation, row))
                .map(BigDecimal::valueOf)
                .reduce(BigDecimal::add)
                .map(v -> v.divide(BigDecimal.valueOf(matrix.getSize()), 20, RoundingMode.HALF_UP))
                .map(BigDecimal::doubleValue)
                .orElseThrow(() -> new RuntimeException("Exception during average bandwidth calculation"));
    }

    public static BigInteger getProfile(final IMatrix matrix) {
        return Stream.iterate(0, a -> a + 1)
                .limit(matrix.getSize())
                .map(row -> getLocalBandwidth(matrix, row))
                .map(BigInteger::valueOf)
                .reduce(BigInteger::add)
                .orElseThrow(() -> new RuntimeException("Exception during average bandwidth calculation"));
    }

    public static BigInteger getProfile(final IMatrix matrix, final IVector permutation) {
        return Stream.iterate(0, a -> a + 1)
                .limit(matrix.getSize())
                .map(row -> getLocalBandwidth(matrix, permutation, row))
                .map(BigInteger::valueOf)
                .reduce(BigInteger::add)
                .orElseThrow(() -> new RuntimeException("Exception during average bandwidth calculation"));
    }
}
