package ru.unn.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.IMatrix;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class LoadMatrix {
    private final static Logger logger = LoggerFactory.getLogger(LoadMatrix.class);

    public static IMatrix loadMatrix(final File file) {
        int firstIndex = getMinimalIndex(file);
        if (firstIndex == Integer.MAX_VALUE || firstIndex == -1) {
            logger.error("Something went wrong during loading.");
            return null;
        }
        try (final Scanner in = new Scanner(file)) {
            String count = null;
            // Get rid of comments
            while (in.hasNextLine() && "%#$".contains((count = in.nextLine()).subSequence(0, 1))) ;
            if (count == null)
                return null;
            int size = Integer.valueOf(Arrays.stream(count.split("\\s+"))
                    .filter(StringUtils::isNotEmpty).findFirst().orElse("0"));
            MatrixBuilder builder = new MatrixBuilder(size);
            while (in.hasNextLine()) {
                String next = nextLine(in);
                if (next != null) {
                    String[] split = Arrays.stream(next.split("\\s+")).filter(StringUtils::isNotEmpty)
                            .toArray(String[]::new);
                    if (split.length < 2)
                        continue;
                    int a = Integer.valueOf(split[0]) - firstIndex;
                    int b = Integer.valueOf(split[1]) - firstIndex;
                    builder.addItem(a, b);
                }
            }
            return builder.buildMatrix();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int getMinimalIndex(final File file) {
        int minimal = Integer.MAX_VALUE;
        try (final Scanner in = new Scanner(file)) {
            // Get rid of comments and statistics
            while (in.hasNextLine() && "%#$".contains(in.nextLine().subSequence(0, 1))) ;
            for (int i = 0; i < 10 && in.hasNextLine(); i++) {
                String next = nextLine(in);
                if (next != null) {
                    String[] split = Arrays.stream(next.split("\\s+")).filter(StringUtils::isNotEmpty)
                            .toArray(String[]::new);
                    if (split.length < 2)
                        continue;
                    int a = Integer.valueOf(split[0]);
                    int b = Integer.valueOf(split[1]);
                    if (a < minimal) {
                        minimal = a;
                    }
                    if (b < minimal) {
                        minimal = b;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            logger.error("File not found.", e);
            minimal = -1;
        }
        return minimal;
    }

    private static String nextLine(Scanner in) {
        String next = null;
        while (in.hasNextLine() && "%#$".contains((next = in.nextLine()).subSequence(0, 1))) ;
        return next == null || "%#$".contains(next.subSequence(0, 1)) ? null : next;
    }
}
