package ru.unn.util;

import ru.unn.IMatrix;
import ru.unn.IOptimizer;
import ru.unn.IVector;
import ru.unn.optimizer.CuthillMckeeScheme;
import ru.unn.optimizer.ReducingWrapper;
import ru.unn.optimizer.local.ConcurrentOptimizationScheme;
import ru.unn.optimizer.local.SubsequentOptimizationScheme;
import ru.unn.optimizer.local.strategy.IOptimizationStrategy;

public class OptimizerBuilder {

    public static IOptimizer createEmptyOptimizer() {
        return new IOptimizer() {
            private IVector result;

            @Override
            public void optimize(IMatrix value, IVector oldResult) {
                result = oldResult;
            }

            @Override
            public IVector getResult() {
                return result;
            }
        };
    }

    public static CuthillMckeeScheme createCutthilMckee(Long timeLimit, int iterationsLimit) {
        CuthillMckeeScheme optimizer = new CuthillMckeeScheme(iterationsLimit);
        optimizer.setTimeLimit(timeLimit);
        return optimizer;
    }

    public static SubsequentOptimizationScheme createSubsequentOptimizer(IOptimizationStrategy optimizationStrategy) {
        return createSubsequentOptimizer(optimizationStrategy, null, null);
    }

    public static SubsequentOptimizationScheme createSubsequentOptimizer(IOptimizationStrategy optimizationStrategy, Long timeLimit, Integer tactsLimit) {
        SubsequentOptimizationScheme optimizer = new SubsequentOptimizationScheme(optimizationStrategy);
        optimizer.setTactLimit(tactsLimit);
        optimizer.setTimeLimit(timeLimit);
        return optimizer;
    }

    public static ConcurrentOptimizationScheme createParallelOptimizer(IOptimizationStrategy optimizationStrategy, int threads) {
        return createParallelOptimizer(optimizationStrategy, null, null, threads);
    }

    public static ConcurrentOptimizationScheme createParallelOptimizer(IOptimizationStrategy optimizationStrategy, Long timeLimit, Integer iterationsLimit, int threads) {
        ConcurrentOptimizationScheme optimizer = new ConcurrentOptimizationScheme(optimizationStrategy, threads);
        optimizer.setIterationsLimit(iterationsLimit);
        optimizer.setTimeLimit(timeLimit);
        return optimizer;
    }

    public static ReducingWrapper createReducingWrapper(IOptimizer internalOptimizer, IOptimizationStrategy optimizationStrategy) {
        return createReducingWrapper(internalOptimizer, optimizationStrategy, 4, 10, 30, 100);
    }

    public static ReducingWrapper createReducingWrapper(IOptimizer internalOptimizer, IOptimizationStrategy optimizationStrategy, Integer threads, Integer iterationsLimit, Integer threshold, Integer reductionLimit) {
        return new ReducingWrapper(internalOptimizer, iterationsLimit, threads, threshold, reductionLimit, optimizationStrategy);
    }

}
