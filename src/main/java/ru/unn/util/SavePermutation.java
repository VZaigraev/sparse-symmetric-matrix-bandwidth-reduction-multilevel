package ru.unn.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.IVector;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class SavePermutation {
    private static final Logger logger = LoggerFactory.getLogger(SavePermutation.class);

    public static boolean save(String filename, IVector vector) {
        File file = new File("./result/" + filename);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            if (vector != null) {
                int N = vector.getSize();
                for (int i = 0; i < vector.getSize(); i++) {
                    writer.write(String.valueOf(vector.get(i)));
                    if (i < N - 1) {
                        writer.newLine();
                    }
                }
            }
        } catch (IOException e) {
            logger.error("File writing failed", e);
            return false;
        }
        return true;
    }
}
