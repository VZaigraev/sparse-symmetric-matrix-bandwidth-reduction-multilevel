package ru.unn.util;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public abstract class ConfigurationHolder {
    public static Configuration configuration;

    public static boolean init(String filename) {
        try {
            configuration = new PropertiesConfiguration(filename);
        } catch (ConfigurationException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
