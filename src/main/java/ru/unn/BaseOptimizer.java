package ru.unn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public abstract class BaseOptimizer implements IOptimizer {
    private static final Logger logger = LoggerFactory.getLogger(BaseOptimizer.class);
    private final AtomicBoolean activated = new AtomicBoolean(false);
    protected final AtomicReference<IVector> result = new AtomicReference<>();
    private final AtomicReference<Exception> exception = new AtomicReference<>();
    protected boolean loggerEnabled = true;
    private Long timeLimit;
    private long endTime;

    @Override
    public void optimize(IMatrix value, IVector oldResult) {
        result.set(oldResult);
        initEndTime();
        if (activated.getAndSet(true)) {
            logger.error("{}: Optimizer has been activated earlier", this.getClass().getSimpleName());
        } else {
            try {
                internalOptimize(value, oldResult);
            } catch (Exception e) {
                exception.set(e);
                logger.error(this.getClass().getSimpleName() + "Exception caught.", e);
            }
        }
    }

    protected abstract void internalOptimize(IMatrix value, IVector oldResult) throws Exception;

    private void initEndTime() {
        endTime = timeLimit == null ? Long.MAX_VALUE : System.currentTimeMillis() + timeLimit * 1000;
    }

    protected boolean isActive() {
        return System.currentTimeMillis() < endTime;
    }

    @Override
    public IVector getResult() {
        return result.get();
    }

    public Exception getException() {
        return exception.get();
    }

    public void setLoggerEnabled(boolean loggerEnabled) {
        this.loggerEnabled = loggerEnabled;
    }

    public void setTimeLimit(Long timeLimit) {
        this.timeLimit = timeLimit;
    }
}
