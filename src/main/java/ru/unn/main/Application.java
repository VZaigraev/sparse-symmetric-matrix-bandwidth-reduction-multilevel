package ru.unn.main;

import org.apache.commons.cli.*;
import ru.unn.IExperiment;
import ru.unn.ILoader;
import ru.unn.experiment.ConfiguredMultilevelExperiment;
import ru.unn.experiment.MultilevelExperiment;
import ru.unn.loader.DirectoryMatrixLoader;
import ru.unn.loader.ManualMatrixLoader;
import ru.unn.saver.MatrixStatisticSaver;
import ru.unn.util.ConfigurationHolder;
import ru.unn.util.SavePermutation;

import java.io.File;

public class Application {
    private static final String DEFAULT_DIRECTORY = "./test/";
    private static final String DEFAULT_CONFIG = "./config.properties";
    private static final String SAVE_PERMUTATION_OPT = "s";
    private static final String GRAPHICAL_OPT = "g";
    private static final String DIRECTORY_OPT = "d";
    private static final String CONFIG_OPT = "c";

    public static void main(final String[] args) {
        final CommandLine commandLine = createCommandLine(args);
        initDir();
        boolean configured = ConfigurationHolder.init(commandLine.getOptionValue(CONFIG_OPT, DEFAULT_CONFIG));

        ILoader loader;
        if (commandLine.hasOption(GRAPHICAL_OPT)) {
            loader = new ManualMatrixLoader();
        } else {
            final String name = commandLine.getOptionValue(DIRECTORY_OPT, DEFAULT_DIRECTORY);
            loader = new DirectoryMatrixLoader(name);
        }

        MatrixStatisticSaver saver = new MatrixStatisticSaver(MultilevelExperiment.class,
                MultilevelExperiment.headers);

        boolean savePermutation = commandLine.hasOption(SAVE_PERMUTATION_OPT);

        runExperiment(configured, loader, saver, savePermutation);
    }

    private static void initDir() {
        File dir = new File("./result");
        dir.mkdir();
    }

    private static void runExperiment(final boolean configured,
                                      final ILoader loader,
                                      final MatrixStatisticSaver saver,
                                      final boolean savePermutation) {
        loader.forEach(e -> {
            System.out.println(String.format("Processing %s", e.getName()));
            IExperiment experiment;
            if (configured) {
                experiment = new ConfiguredMultilevelExperiment();
            } else {
                experiment = new MultilevelExperiment();
            }
            experiment.apply(e);
            saver.save(experiment.collect());
            if (savePermutation) {
                final String filename = String.format("./result/perm-%s-%d.v",
                        e.getName(),
                        System.currentTimeMillis());
                SavePermutation.save(filename, experiment.getPermutation());
            }
            System.out.println(String.format("Finished %s", e.getName()));
        });
    }

    private static CommandLine createCommandLine(final String[] args) {
        final CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = null;
        try {
            commandLine = parser.parse(buildCmdLineOptions(), args);
        } catch (ParseException e) {
            System.out.println("Error: " + e.getLocalizedMessage());
            e.printStackTrace();
            System.exit(e.hashCode());
        }
        return commandLine;
    }

    private static Options buildCmdLineOptions() {
        Options options = new Options();
        options.addOption(SAVE_PERMUTATION_OPT, false, "Save permutation");
        options.addOption(CONFIG_OPT, true, "Configuration file");
        OptionGroup optionGroup = new OptionGroup();
        optionGroup.setRequired(false);
        optionGroup.addOption(new Option(GRAPHICAL_OPT, false, "Graphical file chooser"));
        optionGroup.addOption(new Option(DIRECTORY_OPT, true, "Directory"));
        options.addOptionGroup(optionGroup);
        return options;
    }
}
