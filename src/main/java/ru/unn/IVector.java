package ru.unn;

import java.io.Serializable;
import java.util.Comparator;

public interface IVector extends Serializable {
    int getSize();

    int indexOf(final int value);

    int get(final int value);

    void swap(final int a, final int b);

    void sort(final int from, final int to, final Comparator<Integer> comparator);

    int[] copyOfRange(final int from, final int to);

    IVector copyInstance();
}
