package ru.unn.experiment;

import ru.unn.IMatrix;
import ru.unn.optimizer.CuthillMckeeScheme;
import ru.unn.optimizer.local.ConcurrentOptimizationScheme;
import ru.unn.optimizer.local.strategy.AverageReductionStrategy;
import ru.unn.util.OptimizerBuilder;
import ru.unn.util.PermutationHolder;

public class ParallelExperiment extends BaseExperiment {
    private static final String CM_BAND = "CM Bandwidth";
    private static final String CM_AVER = "CM Average";
    public static final String[] headers = new String[]{
            NAME,
            SIZE,
            INIT_BAND,
            INIT_AVER,
            CM_BAND,
            CM_AVER,
            RES_BAND,
            RES_AVER,
            RES_TIME
    };

    @Override
    public PermutationHolder process(final IMatrix matrix, PermutationHolder holder) {
        CuthillMckeeScheme cm1 = OptimizerBuilder.createCutthilMckee(null, 1000);
        cm1.optimize(matrix, holder.getVector());
        holder = new PermutationHolder(matrix, cm1.getResult());
        register(CM_BAND, holder.getBandwidth());
        register(CM_AVER, holder.getAverage());

        ConcurrentOptimizationScheme opt = OptimizerBuilder.createParallelOptimizer(new AverageReductionStrategy(),
                60000L, null, 1);
        opt.optimize(matrix, holder.getVector());
        return new PermutationHolder(matrix, opt.getResult());
    }
}
