package ru.unn.experiment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.unn.IExperiment;
import ru.unn.IMatrix;
import ru.unn.IVector;
import ru.unn.util.EntityWithName;
import ru.unn.util.PermutationHolder;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseExperiment implements IExperiment {
    private static final Logger logger = LoggerFactory.getLogger(BaseExperiment.class);
    protected static final String NAME = "Name";
    protected static final String SIZE = "Size";
    protected static final String INIT_BAND = "Bandwidth";
    protected static final String INIT_AVER = "Average";
    protected static final String RES_BAND = "Result Bandwidth";
    protected static final String RES_AVER = "Result Average";
    protected static final String RES_TIME = "Time";
    protected final Map<String, String> statistics = new HashMap<>();
    protected PermutationHolder result;
    protected long startTime;

    protected abstract PermutationHolder process(final IMatrix matrix, PermutationHolder holder);

    @Override
    public void apply(final EntityWithName<IMatrix> data) {
        if (data != null) {
            logger.info(String.format("\n###########################################" +
                    "\nProcessing entity: %s" +
                    "\n###########################################", data.getName()));
            final IMatrix matrix = data.getData();
            PermutationHolder holder = new PermutationHolder(matrix, matrix.getInitialPermutation());
            register(NAME, data.getName());
            register(SIZE, matrix.getSize());
            register(INIT_BAND, holder.getBandwidth());
            register(INIT_AVER, holder.getAverage());
            startTimeCount();

            result = process(matrix, holder);
            register(RES_BAND, result.getBandwidth());
            register(RES_AVER, result.getAverage());
            register(RES_TIME, getExecutionTime());

            logger.info(String.format("\n###########################################" +
                    "\nFinished entity: %s" +
                    "\n###########################################", data.getName()));
        }
    }

    @Override
    public IVector getPermutation() {
        return result == null ? null : result.getVector();
    }

    @Override
    public Map<String, String> collect() {
        return statistics;
    }

    protected void startTimeCount() {
        startTime = System.currentTimeMillis();
    }

    protected long getExecutionTime() {
        return System.currentTimeMillis() - startTime;
    }

    protected void register(String header, Object value) {
        statistics.put(header, String.valueOf(value));
        logger.info("Registering {} : {}", header, String.valueOf(value));
    }
}
