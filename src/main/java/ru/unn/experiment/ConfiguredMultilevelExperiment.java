package ru.unn.experiment;

import ru.unn.BaseOptimizer;
import ru.unn.IMatrix;
import ru.unn.IOptimizer;
import ru.unn.IVector;
import ru.unn.optimizer.CuthillMckeeScheme;
import ru.unn.optimizer.ReducingWrapper;
import ru.unn.optimizer.local.ConcurrentOptimizationScheme;
import ru.unn.optimizer.local.strategy.BandwidthReductionStrategy;
import ru.unn.util.OptimizerBuilder;
import ru.unn.util.PermutationHolder;

import static ru.unn.util.ConfigurationHolder.configuration;

public class ConfiguredMultilevelExperiment extends MultilevelExperiment {
    private static final String CM_SECONDS = "cm.seconds";
    private static final String CM_ITERS = "cm.iterations";
    private static final String RED_THREADS = "reduction.threads";
    private static final String RED_ITERS = "reduction.iterations";
    private static final String RED_THRESHOLD = "reduction.threshold";
    private static final String RED_MIN = "reduction.minimal";
    private static final String OPT_THREADS = "optimization.threads";
    private static final String OPT_ITERS = "optimization.iterations";
    private static final String OPT_SECONDS = "optimization.seconds";

    @Override
    public PermutationHolder process(final IMatrix matrix, PermutationHolder holder) {
        PermutationHolder tempHolder;

        CuthillMckeeScheme cm = OptimizerBuilder.createCutthilMckee(configuration.getLong(CM_SECONDS, null),
                configuration.getInteger(CM_ITERS, null));
        cm.optimize(matrix, holder.getVector());
        tempHolder = new PermutationHolder(matrix, cm.getResult());
        register(CM_BAND, tempHolder.getBandwidth());
        register(CM_AVER, tempHolder.getAverage());
        holder = PermutationHolder.bestByBandwidth(holder, tempHolder);

        IOptimizer internal = new BaseOptimizer() {
            @Override
            protected void internalOptimize(IMatrix matrix, IVector oldResult) {
                PermutationHolder holder = new PermutationHolder(matrix, oldResult);
                PermutationHolder tempHolder;
                register(REDUCED_SIZE, matrix.getSize());
                register(REDUCED_BAND, holder.getBandwidth());
                register(REDUCED_AVER, holder.getAverage());

                ConcurrentOptimizationScheme opt = OptimizerBuilder.createParallelOptimizer(new BandwidthReductionStrategy(),
                        configuration.getLong(OPT_SECONDS, null),
                        configuration.getInteger(OPT_ITERS, null),
                        configuration.getInteger(OPT_THREADS, null));
                opt.optimize(matrix, holder.getVector());
                tempHolder = new PermutationHolder(matrix, opt.getResult());
                register(OPT_BAND, tempHolder.getBandwidth());
                register(OPT_AVER, tempHolder.getAverage());
                holder = PermutationHolder.bestByBandwidth(holder, tempHolder);
                result.set(holder.getVector());
            }
        };
        ReducingWrapper red = OptimizerBuilder.createReducingWrapper(internal,
                new BandwidthReductionStrategy(),
                configuration.getInteger(RED_THREADS, null),
                configuration.getInteger(RED_ITERS, null),
                configuration.getInteger(RED_THRESHOLD, null),
                configuration.getInteger(RED_MIN, null));
        red.optimize(matrix, holder.getVector());
        tempHolder = new PermutationHolder(matrix, red.getResult());
        register(RESTORED_BAND, tempHolder.getBandwidth());
        register(RESTORED_AVER, tempHolder.getAverage());
        holder = PermutationHolder.bestByBandwidth(tempHolder, holder);

        return holder;
    }
}
