package ru.unn.experiment;

import ru.unn.BaseOptimizer;
import ru.unn.IMatrix;
import ru.unn.IOptimizer;
import ru.unn.IVector;
import ru.unn.optimizer.CuthillMckeeScheme;
import ru.unn.optimizer.ReducingWrapper;
import ru.unn.optimizer.local.ConcurrentOptimizationScheme;
import ru.unn.optimizer.local.strategy.BandwidthReductionStrategy;
import ru.unn.util.OptimizerBuilder;
import ru.unn.util.PermutationHolder;

public class MultilevelExperiment extends BaseExperiment {
    protected static final String CM_BAND = "CM Bandwidth";
    protected static final String CM_AVER = "CM Average";
    protected static final String REDUCED_SIZE = "Reduced Size";
    protected static final String REDUCED_BAND = "Reduced Bandwidth";
    protected static final String REDUCED_AVER = "Reduced Average";
    protected static final String OPT_BAND = "Optimizer Bandwidth";
    protected static final String OPT_AVER = "Optimizer Average";
    protected static final String RESTORED_BAND = "Restored Bandwidth";
    protected static final String RESTORED_AVER = "Restored Average";
    public static final String[] headers = new String[]{
            NAME,
            SIZE,
            INIT_BAND,
            INIT_AVER,
            CM_BAND,
            CM_AVER,
            REDUCED_SIZE,
            REDUCED_BAND,
            REDUCED_AVER,
            OPT_BAND,
            OPT_AVER,
            RESTORED_BAND,
            RESTORED_AVER,
            RES_BAND,
            RES_AVER,
            RES_TIME
    };

    @Override
    public PermutationHolder process(final IMatrix matrix, PermutationHolder holder) {
        PermutationHolder tempHolder;

        CuthillMckeeScheme cm = OptimizerBuilder.createCutthilMckee(null, 1000);
        cm.optimize(matrix, holder.getVector());
        tempHolder = new PermutationHolder(matrix, cm.getResult());
        register(CM_BAND, tempHolder.getBandwidth());
        register(CM_AVER, tempHolder.getAverage());
        holder = PermutationHolder.bestByBandwidth(holder, tempHolder);

        IOptimizer internal = new BaseOptimizer() {
            @Override
            protected void internalOptimize(IMatrix matrix, IVector oldResult) {
                PermutationHolder holder = new PermutationHolder(matrix, oldResult);
                PermutationHolder tempHolder;
                register(REDUCED_SIZE, matrix.getSize());
                register(REDUCED_BAND, holder.getBandwidth());
                register(REDUCED_AVER, holder.getAverage());

                ConcurrentOptimizationScheme opt = OptimizerBuilder.createParallelOptimizer(new BandwidthReductionStrategy(),
                        300L, null, 4);
                opt.optimize(matrix, holder.getVector());
                tempHolder = new PermutationHolder(matrix, opt.getResult());
                register(OPT_BAND, tempHolder.getBandwidth());
                register(OPT_AVER, tempHolder.getAverage());
                holder = PermutationHolder.bestByBandwidth(holder, tempHolder);
                result.set(holder.getVector());
            }
        };
        ReducingWrapper red = OptimizerBuilder.createReducingWrapper(internal,
                new BandwidthReductionStrategy(), 4, 10, 2, 100);
        red.optimize(matrix, holder.getVector());
        tempHolder = new PermutationHolder(matrix, red.getResult());
        register(RESTORED_BAND, tempHolder.getBandwidth());
        register(RESTORED_AVER, tempHolder.getAverage());
        holder = PermutationHolder.bestByBandwidth(tempHolder, holder);

        return holder;
    }
}
